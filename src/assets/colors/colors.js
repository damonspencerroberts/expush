const colors = {
  background: "#3c493fff",
  lightOlive: "#7e8d85ff",
  ashOlive: "#b3bfb8ff",
  mintCream: "#f0f7f4ff",
  lightGreen: "#a2e3c4ff"
}

export default colors;
