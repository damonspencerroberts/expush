import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import TopHeader from "../components/top-header/top-header";
import NavBottom from "../components/nav-bottom/nav-bottom";
import ContainerDiv from "../components/container/container";
import LogoSecond from "../components/logo-second/logo-seconds";
import colors from "../assets/colors/colors";
import ChoiceHeader from "../components/header/choice-header";
import dataExercise from "./data/data-exercise";
import { useState } from 'react';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    height: '100%',
    width: '100%'
  },
});

export default function ExerciseScreen(props) {
  const [exercise, setExercise] = useState("");

  const changePage = (where) => {
    props.navigation.navigate(where);
  }

  const changePageWithParams = (where, choice) => {
    setExercise(choice)
    console.log(choice)
    props.navigation.navigate(where, {
      exercise: choice
    });
  }

  const exerciseChoices = dataExercise.map((exercise, index) => {
    return (
      <ChoiceHeader 
        key={index}
        subHeader={exercise.exercise}
        textWords={exercise.words}
        nav={changePageWithParams}
      />
    );
  });

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <NavBottom 
          nav={changePage}
          profile="Profile"
          exercise="Exercise"
          about="About"
        />
        <LogoSecond />
        <View style={styles.container}>
          <ContainerDiv>
            <TopHeader 
              header="Exercises"
              lightText="Choose one or more."
            />
            {exerciseChoices}
          </ContainerDiv>
        </View>
      </ScrollView>
      <StatusBar style="auto" />
    </View>
  );
}