import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import TopHeader from "../components/top-header/top-header";
import NavBottom from "../components/nav-bottom/nav-bottom";
import ContainerDiv from "../components/container/container";
import LogoSecond from "../components/logo-second/logo-seconds";
import colors from "../assets/colors/colors";
import { Picker } from "@react-native-picker/picker";
import dataReps from "./data/data-reps";
import CustomButton from "../components/button/button";
import PickerDiv from "../components/picker/picker";

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    height: '100%',
    width: '100%'
  },
});

export default function ExerciseScreen(props) {
  const [selectedRep, setSelectedRep] = useState("5");
  const [selectedAlert, setSelectedAlert] = useState("3");
  const changePage = (where) => {
    props.navigation.navigate(where);
  }
  const choice = props.route.params.exercise;

  const repChoices = dataReps[choice].reps.map((rep, index) => {
    return <Picker.Item key={index} label={rep} value={rep} color={colors.mintCream} />
  });

  const alertChoices = dataReps.alerts.map((alert, index) => {
    return <Picker.Item key={index} label={alert} value={alert} color={colors.mintCream} />
  });

  const unit = dataReps[choice].unit

  const handleGenerateItems = (where) => {
    const values = {
      reps: selectedRep,
      alerts: selectedAlert,
      choice: choice,
      unit: unit
    }

    props.navigation.navigate(where, {
      values: values
    });
  }

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <NavBottom 
          nav={changePage}
          profile="Profile"
          exercise="Exercise"
          about="About"
        />
        <LogoSecond />
        <View style={styles.container}>
          <ContainerDiv>
            <TopHeader 
              header={choice}
              lightText="Edit your preferences below."
            />
            <PickerDiv 
              unit={`How many ${dataReps[choice].unit} per alert?`}
              color={colors.mintCream}
              selected={selectedRep}
              setSelected={setSelectedRep}
              choice={repChoices}
            />
            <PickerDiv 
              unit="How many alerts per day?"
              color={colors.mintCream}
              selected={selectedAlert}
              setSelected={setSelectedAlert}
              choice={alertChoices}
            />
            <CustomButton 
              content="Generate"
              nav={handleGenerateItems}
              where="Profile"
              fontColor={colors.lightGreen}
              backButton={colors.background}
            />
          </ContainerDiv>
        </View>
      </ScrollView>
      <StatusBar style="auto" />
    </View>
  );
}