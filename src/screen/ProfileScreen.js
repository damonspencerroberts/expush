import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import TopHeader from "../components/top-header/top-header";
import NavBottom from "../components/nav-bottom/nav-bottom";
import ContainerDiv from "../components/container/container";
import LogoSecond from "../components/logo-second/logo-seconds";
import colors from "../assets/colors/colors";
import SubHeader from "../components/header/sub-header";
import Card from "../components/cards/main-card";

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    height: '100%',
    width: '100%'
  },
});

export default function ExerciseScreen(props) {
  let values;
  if (props.route.params !== undefined) {
    values = props.route.params.values;
  }

  const changePage = (where) => {
    props.navigation.navigate(where);
  }
  // <View>
  // <Text>{values !== undefined ? values.reps : null}</Text>
  // <Text>{values !== undefined ? values.alerts : null}</Text>
  // <Text>{values !== undefined ? values.choice : null}</Text>
  // </View>
  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <NavBottom 
          nav={changePage}
          profile="Profile"
          exercise="Exercise"
          about="About"
        />
        <LogoSecond />
        <View style={styles.container}>
          <ContainerDiv>
            <TopHeader 
              header="Welcome, John"
              lightText="Track your progress."
            />
            <SubHeader
              text="Your Exercises"
              margTop={20}
            />
            <Card 
              cardText1={values !== undefined ? values.choice : null}
              cardText2={`${values !== undefined ? values.reps : null} ${values.unit} `}
              cardText3={`${values !== undefined ? values.alerts : null}x per day`}
            />
          </ContainerDiv>
        </View>
      </ScrollView>
      <StatusBar style="auto" />
    </View>
  );
}