import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import TopHeader from "../components/top-header/top-header";
import NavBottom from "../components/nav-bottom/nav-bottom";
import ContainerDiv from "../components/container/container";
import LogoSecond from "../components/logo-second/logo-seconds";
import colors from "../assets/colors/colors";

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    height: '100%',
    width: '100%'
  },
});

export default function ExerciseScreen(props) {
  const changePage = (where) => {
    props.navigation.navigate(where);
  }

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <NavBottom 
          nav={changePage}
          profile="Profile"
          exercise="Exercise"
          about="About"
        />
        <LogoSecond />
        <View style={styles.container}>
          <ContainerDiv>
            <TopHeader 
              header="About Expush"
              lightText="Five step process."
            />
          </ContainerDiv>
        </View>
      </ScrollView>
      <StatusBar style="auto" />
    </View>
  );
}