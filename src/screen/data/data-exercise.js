const dataExercise = [
  {
    num: "1",
    exercise: "PUSH UPS",
    words: "The simple push up. You choose 5, 10, or 15 for each rep."
  },
  {
    num: "2",
    exercise: "SQUATS",
    words: "The simple squat. You choose 10, 20 or 30 for each rep."
  },
  {
    num: "3",
    exercise: "PLANK",
    words: "The simple plank. You choose 20 seconds, 40 seconds or 1 minute for each rep."
  }
];

export default dataExercise;