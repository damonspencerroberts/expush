const dataReps = {
  "PUSH UPS": {
    reps: ["5", "10", "15"],
    unit: "Reps"
  },
  "SQUATS": {
    reps: ["10", "20", "30"],
    unit: "Reps"
  },
  "PLANK": {
    reps: ["20", "40", "60"],
    unit: "Seconds"
  },
  alerts: ["3", "5", "7"]
}

export default dataReps;
