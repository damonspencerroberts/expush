import { StatusBar } from 'expo-status-bar';
import React, { Fragment } from 'react';
import { StyleSheet, ScrollView, View, Button } from 'react-native';
import colors from "../assets/colors/colors";
import ContainerDiv from "../components/container/container";
import Logo from "../components/logo/logo";
import Header from "../components/header/header";
import CustomButton from "../components/button/button";
import NavBottom from "../components/nav-bottom/nav-bottom";
import Words from "../components/text/text";

export default function HomeScreen(props) {
  const changePage = (where) => {
    props.navigation.navigate(where);
  }

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <NavBottom 
          nav={changePage}
          profile="Profile"
          exercise="Exercise"
          about="About"
        />
        <Logo />
        <View style={styles.container}>
          <ContainerDiv>
            <Header text="Welcome" />
            <Words 
              words="We want you to get strong without even realizing."
              color={colors.mintCream}
            />
            <Words 
              words="We know working out is hard."
              color={colors.mintCream}
            />
            <Words 
              words="That’s why we notify YOU to complete very simple exercies throughout the day."
              color={colors.lightGreen}
            />
            <CustomButton 
              content="How it works"
              nav={changePage}
              where="About"
              fontColor={colors.lightGreen}
              backButton={colors.background}
            />
            <CustomButton 
              content="Sign up"
              nav={changePage}
              where="Signup"
              fontColor={colors.background}
              backButton={colors.lightGreen}
            />
            <StatusBar style="auto" />
          </ContainerDiv>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    height: '100%',
    width: '100%'
  },
});