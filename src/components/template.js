import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from "prop-types";

function name({ text }) {
  return (
    <View>
      <Text>{text}</Text>
    </View>
  );
}

name.propTypes = {
  text: PropTypes.string
};

export default name;