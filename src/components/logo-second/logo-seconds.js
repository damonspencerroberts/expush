import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 150
  }
})

function LogoSecond() {
  return (
    <View>
      <Image source={require("../../assets/images/logo2.png")} style={styles.image} />
    </View>
  );
}

export default LogoSecond;