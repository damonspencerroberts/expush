import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from "prop-types";
import TextDiv from "../text/text";
import { Picker } from "@react-native-picker/picker";

function PickerDiv({ unit, color, selected, setSelected, choice }) {

  return (
    <View>
      <TextDiv 
        words={unit}
        color={color}
        margBottom={0}
      />
      <Picker
        style={{
          color: 'white' 
        }}
        selectedValue={selected}
        onValueChange={(itemValue, itemIndex) =>
          setSelected(itemValue)
        }>
        {choice}
      </Picker>
    </View>
  );
}

PickerDiv.propTypes = {
  unit: PropTypes.string,
  color: PropTypes.string,
  selected: PropTypes.string,
  setSelected: PropTypes.func,
  choice: PropTypes.any
};

export default PickerDiv;