import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 150
  }
})

function Logo() {
  return (
    <View>
      <Image source={require("../../assets/images/logo1.png")} style={styles.image} />
    </View>
  );
}

export default Logo;