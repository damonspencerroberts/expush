import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from "prop-types";
import colors from "../../assets/colors/colors";
import { useFonts, MontserratAlternates_300Light } from "@expo-google-fonts/dev";
import AppLoading from 'expo-app-loading';

function LightText({ text }) {
  let [fontLoading, error] = useFonts({
    MontserratAlternates_300Light
  });

  if (!fontLoading) {
    return <AppLoading />
  }

  return (
    <View style={{
      marginTop: 10
    }}>
      <Text style={{
        opacity: 0.7,
        color: colors.lightOlive,
        fontSize: 22,
        fontFamily: "MontserratAlternates_300Light"
      }}>{text}</Text>
    </View>
  );
}

LightText.propTypes = {
  text: PropTypes.string
};

export default LightText;