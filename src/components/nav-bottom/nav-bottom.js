import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from "prop-types";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faUser, faNewspaper, faDumbbell } from "@fortawesome/free-solid-svg-icons";
import { useFonts, Nunito_600SemiBold } from "@expo-google-fonts/dev";
import colors from "../../assets/colors/colors";
import AppLoading from 'expo-app-loading';

function NavBottom({ nav, profile, exercise, about }) {
  let [fontsLoaded, error] = useFonts({
    Nunito_600SemiBold,
  });

  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <View style={{
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'fixed',
      backgroundColor: colors.background
    }}>
      <TouchableOpacity 
        onPress={() => nav(profile)}
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: colors.lightGreen,
          padding: 20,
          borderRightWidth: 2,
          borderColor: colors.background,
          borderBottomLeftRadius: 15
        }}
      > 
        <FontAwesomeIcon 
          icon={ faUser }
          style={{
            color: colors.background
          }}
          />
        <Text style={{
          marginTop: 10,
          fontFamily: "Nunito_600SemiBold",
          color: colors.background
        }}>Profile</Text>
      </TouchableOpacity>

      <TouchableOpacity 
        onPress={() => nav(exercise)}
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: colors.lightGreen,
          padding: 20,
          borderColor: colors.background
        }}
      > 
        <FontAwesomeIcon 
          icon={ faDumbbell }
          style={{
            color: colors.background
          }}
        />
        <Text style={{
          marginTop: 10,
          fontFamily: "Nunito_600SemiBold",
          color: colors.background
        }}>Exercises</Text>
      </TouchableOpacity>

      <TouchableOpacity 
        onPress={() => nav(about)}
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: colors.lightGreen,
          padding: 20,
          borderLeftWidth: 2,
          borderColor: colors.background,
          borderBottomRightRadius: 15
        }}
      > 
        <FontAwesomeIcon 
          icon={ faNewspaper }
          style={{
            color: colors.background
          }}
          />
        <Text style={{
          marginTop: 10,
          fontFamily: "Nunito_600SemiBold",
          color: colors.background
        }}>About</Text>
      </TouchableOpacity>
    </View>
  );
}

NavBottom.propTypes = {
  nav: PropTypes.func,
  profile: PropTypes.string,
  exercise: PropTypes.string, 
  about: PropTypes.string
};

export default NavBottom;