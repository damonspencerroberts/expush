import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from "prop-types";
import AppLoading from "expo-app-loading";
import colors from "../../assets/colors/colors";
import {
  useFonts,
  Lato_700Bold
 } from "@expo-google-fonts/dev";

function SubHeader({ text, flexGrow, margTop }) {
  let [fontsLoaded, error] = useFonts({
    Lato_700Bold,
  });

  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <View style={{
      flexGrow: flexGrow,
      marginTop: margTop
    }}>
      <Text style={{
        fontFamily: "Lato_700Bold", 
        fontSize: 40, 
        color: colors.lightGreen}}>{text}</Text>
    </View>
  );
}

SubHeader.propTypes = {
  text: PropTypes.string,
  flexGrow: PropTypes.number,
  margTop: PropTypes.number
};

export default SubHeader;