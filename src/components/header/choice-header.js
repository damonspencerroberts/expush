import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from "prop-types";
import SubHeader from "./sub-header";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import colors from "../../assets/colors/colors";
import TextDiv from "../text/text";
import { TouchableOpacity } from 'react-native-gesture-handler';

function ChoiceHeader({ subHeader, textWords, nav }) {
  return (
    <View>
      <View style={{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 25,
        marginBottom: 10
      }}>
        <SubHeader text={subHeader} flexGrow={1}/>
        <TouchableOpacity onPress={() => nav('Choose', subHeader)}>
          <FontAwesomeIcon 
            icon={faPlus} 
            size={32}
            color={colors.lightOlive}
          />
        </TouchableOpacity>
      </View>
      <TextDiv 
        words={textWords}
        color={colors.mintCream}
      />
    </View>
  );
}

ChoiceHeader.propTypes = {
  subHeader: PropTypes.string,
  textWords: PropTypes.string,
  nav: PropTypes.func
};

export default ChoiceHeader;
