import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from "prop-types";
import AppLoading from "expo-app-loading";
import colors from "../../assets/colors/colors";
import {
  useFonts,
  Nunito_700Bold
 } from "@expo-google-fonts/dev";

function Header({ text }) {
  let [fontsLoaded, error] = useFonts({
    Nunito_700Bold,
  });

  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <View>
      <Text style={{
        fontFamily: "Nunito_700Bold", 
        fontSize: 40, 
        color: colors.lightGreen}}>{text}</Text>
    </View>
  );
}

Header.propTypes = {
  text: PropTypes.string
};

export default Header;