import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from "prop-types";
import Header from "../header/header";
import LightText from "../light-text/light-text";

function TopHeader({ header, lightText }) {
  return (
    <View style ={{
      flex: 1,
      flexDirection: 'column',
      alignItems: 'flex-start'
    }}>
      <Header text = {header} />
      <LightText text = {lightText} />
    </View>
  );
}

TopHeader.propTypes = {
  lightText: PropTypes.string,
  header: PropTypes.string
};

export default TopHeader;