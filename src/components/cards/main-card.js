import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from "prop-types";
import CustomButton from "../button/button";
import colors from '../../assets/colors/colors';
import {useFonts, Lato_700Bold, Nunito_600SemiBold} from "@expo-google-fonts/dev";


function MainCard({ cardText1, cardText2, cardText3 }) {
  let [fontLoading] = useFonts({
    Lato_700Bold,
    Nunito_600SemiBold
  })
  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
      backgroundColor: colors.lightOlive,
      borderWidth: 1,
      borderColor: colors.lightGreen,
      padding: 10,
      borderRadius: 10,
      marginTop: 20
    }}>
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-evenly'
      }}>
        <Text style={{
          color: colors.mintCream,
          fontSize: 25,
          fontFamily: "Lato_700Bold"
        }}>{cardText1}</Text>
        <Text style={{
          color: colors.lightGreen,
          fontSize: 20,
          fontFamily: "Nunito_600SemiBold"
        }}>{cardText2}</Text>
        <Text style={{
          color: colors.ashOlive,
          fontSize: 20,
          fontFamily: "Nunito_600SemiBold"
        }}>{cardText3}</Text>
      </View>
      <View>
        <CustomButton 
          content="Edit"
          fontColor={colors.background}
          backButton={colors.lightGreen}
        />
      </View>
    </View>
  );
}

MainCard.propTypes = {
  cardText1: PropTypes.string,
  cardText2: PropTypes.string,
  cardText3: PropTypes.string
};

export default MainCard;