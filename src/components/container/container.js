import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  ContainerDiv: {
    margin: 40
  }
})

function ContainerDiv({ children }) {
  return (
    <View style={styles.ContainerDiv}>
      { children }
    </View>
  );
}

ContainerDiv.propTypes = {
  children: PropTypes.node.isRequired
};

export default ContainerDiv;