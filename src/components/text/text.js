import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from "prop-types";
import { useFonts, MontserratAlternates_400Regular } from "@expo-google-fonts/dev";
import AppLoading from "expo-app-loading";


function Words({ words, color, margBottom }) {
  let [fontsLoaded, error] = useFonts({
    MontserratAlternates_400Regular,
  });

  if (!fontsLoaded) {
    return <AppLoading />
  }
  return (
    <View style={{
      marginVertical: 20,
      marginBottom: margBottom
    }}>
      <Text style={{
        fontFamily: "MontserratAlternates_400Regular",
        fontSize: 22,
        color: color
      }}>{words}</Text>
    </View>
  );
}

Words.propTypes = {
  words: PropTypes.string,
  color: PropTypes.string,
  margBottom: PropTypes.number
};

export default Words;