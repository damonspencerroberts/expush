import React from 'react';
import { Button, View } from 'react-native';
import PropTypes from "prop-types";
import colors from "../../assets/colors/colors";

function CustomButton({content, nav, where, fontColor, backButton}) {
  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 50,
      borderWidth: 1,
      borderColor: colors.lightGreen,
      backgroundColor: backButton,
      marginVertical: 20,
      paddingVertical: 6
    }}>
      <Button
        color={fontColor}
        title={content}
        onPress={() =>
          nav(where)
        }
      />
    </View>
  );
}

CustomButton.propTypes = {
  content: PropTypes.string,
  nav: PropTypes.func,
  where: PropTypes.string,
  fontColor: PropTypes.string,
  backButton: PropTypes.string
}

export default CustomButton;
